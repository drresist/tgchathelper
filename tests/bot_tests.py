import unittest
from unittest.mock import patch, MagicMock

from telebot import types

import settings
from bot import bot, get_weather


class TestBot(unittest.TestCase):
    def setUp(self):
        self.city = "Moscow"

    def test_get_weather_success(self):
        with patch("requests.get") as mock_get, patch(
            "builtins.open", MagicMock()
        ), patch("json.load", MagicMock()) as mock_load:
            # Mock the icons dictionary
            mock_icons = {
                "01d": "☀️",
                "02d": "🌤️",
                "03d": "⛅",
                "04d": "☁️",
                "09d": "🌧️",
                "10d": "🌦️",
                "11d": "⛈️",
                "13d": "❄️",
                "50d": "🌫️",
            }
            mock_load.return_value = mock_icons

            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = {
                "weather": [{"icon": "01d", "description": "clear sky"}],
                "main": {"temp": 22.0, "feels_like": 20.0},
            }
            result = get_weather(self.city)
            expected = "Погода в [Moscow]: ☀️: 22°C (ощ. 20°C), clear sky"
            self.assertEqual(result, expected)

    def test_get_weather_failure(self):
        with patch("requests.get") as mock_get, patch(
            "builtins.open", MagicMock()
        ), patch("json.load", MagicMock()):
            mock_get.return_value.status_code = 404
            result = get_weather(self.city)
            expected = ""
            self.assertEqual(result, expected)


if __name__ == "__main__":
    # Set the OpenWeatherMap API key for testing
    bot.OW_TOKEN = settings.OW_TOKEN

    unittest.main()
