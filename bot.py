import json
import logging
import sys
import time

import requests
from telebot import TeleBot, logger, types

from settings import *
from info import fetch_city_info, get_weather, fetch_city_image


bot = TeleBot(TG_TOKEN, parse_mode="MARKDOWN")
logger.setLevel(logging.DEBUG)

@bot.message_handler(commands=["start", "help"])
def send_welcome(message):
    """
    Handle /start and /help commands
    """
    bot.reply_to(message, "Howdy, how are you doing?")
    bot.reply_to(message, get_weather("Москва"))


# TODO - add image and city description to inline window
@bot.inline_handler(lambda query: len(query.query) >= 3)
def query_text(inline_query):
    """
    Handle inline queries starting with 'погода'
    """
    city = inline_query.query
    info = fetch_city_info(city)
    logger.info(info)
    answer = fetch_city_image(city)+ get_weather(city) + info
    try:
        r = types.InlineQueryResultArticle(
            "1", city, types.InputTextMessageContent(answer)
        )
        bot.answer_inline_query(inline_query.id, [r])
    except Exception as e:
        print(e)



def main_loop():
    """
    Run the main bot loop
    """
    bot.infinity_polling()
    while 1:
        time.sleep(10)


if __name__ == "__main__":
    try:
        main_loop()
    except KeyboardInterrupt:
        print("\nExiting by user request.\n")
        sys.exit(0)
