import json
import requests

# Fecth city image from wikipedia
def fetch_city_image(city_name):
    url = f"https://ru.wikipedia.org/w/api.php?action=query&format=json&prop=pageimages&piprop=original&pilimit=1&titles={city_name}"
    response = requests.get(url)
    data = response.json()
    if response.status_code == 200:
        page = next(iter(data["query"]["pages"].values()))
        if "original" in page:
            return page["original"]["source"]
        else:
            return ""

def fetch_city_info(city_name):
    """
    Fetches information about a city from the Russian Wikipedia API using the given city name.

    :param city_name: A string representing the name of the city to fetch information for.
    :type city_name: str

    :return: A string containing the information about the city if it exists on Wikipedia, 
             otherwise a string stating that no information is available.
    :rtype: str
    """
    # Retrive from wiki image by request
    
    url = f"https://ru.wikipedia.org/w/api.php?action=query&exsentences=1&format=json&prop=extracts&exintro&explaintext&titles={city_name}"
    response = requests.get(url)
    data = response.json()

    if response.status_code == 200:
        page = next(iter(data["query"]["pages"].values()))
        if "extract" in page:
            return page["extract"]
        else:
            return "No information available for the given city."
    else:
        return "Failed to fetch city information."

def get_weather(city: str) -> str:
    """
    Request weather from openWeatherApi and format
    :param city: City name
    :return: Weather information as a string if successful, otherwise an empty string
    """
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={OW_TOKEN}&lang=ru&units=metric"
    weather_data = requests.get(url)
    with open("icons.json", "r", encoding="utf-8") as f:
        icons = json.load(f)
    if weather_data.status_code == 200:
        weather_data = weather_data.json()
        icon_code = weather_data["weather"][0]["icon"]
        icon = icons[icon_code]
        temperature = int(weather_data["main"]["temp"])
        feels_like = int(weather_data["main"]["feels_like"])
        description = weather_data["weather"][0]["description"]
        text = f"{city} {icon}: {description}, {temperature}°C (ощ. {feels_like}°C)\n"
        return text
    else:
        return ""
